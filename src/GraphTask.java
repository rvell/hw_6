/*
   Tagasiside küsimused:
   1) Kui palju punkte mul võimalik üldse saada selle töö eest on, kui täna sai meetod valmis.
      Lisainfo 1. küsimuse jaoks : Esmane pooliku töö esitus - 26 november 2017, 22:29
   2) Kas aruanne peab olema viimase kaitsmise ajaks (19.12.17) tehtud või võib seda viimistleda natukene pikemalt.
   3) Kas "//for testing purposes" read findFurthestFrom meetodis tuleks kaitsmise ajaks eemaldada või pigem jätta
      need alles ja kood puhastada aruandesse panekuks?
*/

import java.util.*;
import java.util.List;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {

      // Testcase 1
      // Vertex v11 = new Vertex("v11");
      // Graph g1 = new Graph("1 graph test");
      // g1.findFurthestFrom(v11);

      /**
       // Testcase 2
       Vertex v21 = new Vertex("v1");
       Graph g2 = new Graph("2 graph test", v21);
       g2.createVertexList();
       g2.printVertexList();
       System.out.println(g2);
       g2.findFurthestFrom(v21);
      */

      /**
      //Testcase 3
      // Creating the graph.
      Graph g3 = new Graph("3 graph test");
      // Creation of the four vertices of this graph.
      Vertex v31 = g3.createVertex("v31");
      //Sets Vertice v31 as the first vertice of g3 graph.
      g3.setFirstVertex(v31);
      Vertex v32 = g3.createVertex("v32");
      Vertex v33 = g3.createVertex("v33");
      Vertex v34 = g3.createVertex("v34");
      // Creation of the 6 arcs between the vertices, making it a simple graph.
      Arc v31_v32 = g3.createArc("v31_v32", v31, v32);
      Arc v32_v31 = g3.createArc("v32_v31", v32, v31);
      Arc v32_v33 = g3.createArc("v32_v33", v32, v33);
      Arc v33_v32 = g3.createArc("V33_v32", v33, v32);
      Arc v33_v34 = g3.createArc("v33_v34", v33, v34);
      Arc v34_v33 = g3.createArc("v34_v33", v34, v33);
      // Setting the nextArc for Arcs going out from Vertices.
      v31_v32.setArcNextArc(null);
      v32_v31.setArcNextArc(null);
      v32_v33.setArcNextArc(v32_v31);
      v33_v32.setArcNextArc(null);
      v33_v34.setArcNextArc(v33_v32);
      v34_v33.setArcNextArc(null);
      // Revaluating the vertices to have references to firstArc and nextVertice.
      v31 = new Vertex("v31", v32, v31_v32);
      v32 = new Vertex("v32", v33, v32_v33);
      v33 = new Vertex("v33", v34, v33_v34);
      v34 = new Vertex("v34", v33, v34_v33);
      // Distance between the first two vertices.
      v31_v32.setArcInfo(5);
      v32_v31.setArcInfo(5);
      //Distance between second and third vertice.
      v32_v33.setArcInfo(4);
      v33_v32.setArcInfo(4);
      // Distance between third and fourth vertice.
      v33_v34.setArcInfo(3);
      v34_v33.setArcInfo(3);
      // Creating a list of vertices.
      g3.createVertexList();
      // Testing the actual method.
      g3.findFurthestFrom(v34);
      System.out.println(g3);
      */

      /*
      // Testcase 4
      // Creating the graph.
      Graph g4 = new Graph("4th graph test");
      // Creating the vertices.
      Vertex v41 = g4.createVertex("v41");
      g4.setFirstVertex(v41);
      Vertex v42 = g4.createVertex("v42");
      Vertex v43 = g4.createVertex("v43");
      Vertex v44 = g4.createVertex("v44");
      Vertex v45 = g4.createVertex("v45");
      // Creating the Arcs.
      Arc v41_v42 = g4.createArc("v41_v42", v41, v42);
      Arc v41_v44 = g4.createArc("v41_v44", v41, v44);
      Arc v42_v43 = g4.createArc("v42_v43", v42, v43);
      Arc v42_v41 = g4.createArc("v42_v41", v42, v41);
      Arc v43_v42 = g4.createArc("v43_v42", v43, v42);
      Arc v43_v44 = g4.createArc("v43_v44", v43, v44);
      Arc v43_v45 = g4.createArc("v43_v45", v43, v45);
      Arc v44_v41 = g4.createArc("v44_v41", v44, v41);
      Arc v44_v43 = g4.createArc("v44_v43", v44, v43);
      Arc v45_v43 = g4.createArc("v45_v43", v45, v43);
      // Setting the firstArc in each Arc going out of a Vertice.
      v41_v42.setArcNextArc(null);
      v41_v44.setArcNextArc(v41_v42);
      v42_v41.setArcNextArc(null);
      v42_v43.setArcNextArc(v42_v41);
      v43_v42.setArcNextArc(null);
      v43_v44.setArcNextArc(v43_v42);
      v43_v45.setArcNextArc(v43_v44);
      v44_v41.setArcNextArc(null);
      v44_v43.setArcNextArc(v44_v41);
      v45_v43.setArcNextArc(null);
      // Referencing firstArc and nextVertex for Vertices.
      v41 = new Vertex("v41", v42, v41_v44);
      v42 = new Vertex("v42", v43, v42_v43);
      v43 = new Vertex("v43", v44, v43_v45);
      v44 = new Vertex("v44", v45, v44_v43);
      v45 = new Vertex("v45", null, v45_v43);
      // Setting distances between vertices (length of arcs).
      v41_v42.setArcInfo(10);
      v42_v41.setArcInfo(10);
      v42_v43.setArcInfo(5);
      v43_v42.setArcInfo(5);
      v43_v44.setArcInfo(6);
      v44_v43.setArcInfo(6);
      v44_v41.setArcInfo(8);
      v41_v44.setArcInfo(8);
      v45_v43.setArcInfo(1);
      v43_v45.setArcInfo(1);
      // Creating a list of vertices for this list.
      g4.createVertexList();
      // Testing the method.
      System.out.println(g4.findFurthestFrom(v45));
      System.out.println(g4);
      */

      /*
       * findFurthestFrom doesn't work with createRandomGraph method.
       * The 5th graph test shows that theoretically if createRandomGraph method is changed to add
       * int values to arc.info field it would be compatible with findFurthestFrom method.
       */


      Graph g5 = new Graph ("5th graph test");
      g5.createRandomSimpleGraph(2500, 2500);
      g5.createVertexList();
      Vertex v51 = g5.firstVertex;
      g5.findFurthestFrom(v51);
      System.out.println(g5);

      // System.out.print("All tests have completed, end of run.");

   }

   /**
    * Vertex represents a single point in the graph
    */
   class Vertex {

      private String vertexID;
      private Vertex nextVertex;
      private Arc firstArc;
      private int info = 0;
      // You can add more fields, if needed
      private Vertex previousVertex;
      private List<Vertex> vertexNeighbours = new ArrayList<>();
      private List<Arc> vertexArcList = new ArrayList<>();
      

      Vertex(String s, Vertex v, Arc e) {
         vertexID = s;
         nextVertex = v;
         firstArc = e;

      }

      Vertex(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return vertexID;
      }


      // TODO!!! Your Vertex methods here!

      /**
       * method for accessing the VertexInfo.
       * @return
       */
      public int getVertexInfo() {
         return info;
      }

      /**
       * Method for setting the nextVertex of a vertex
       * @param nextVertex
       */
      public void setNextVertex(Vertex nextVertex) {
         this.nextVertex = nextVertex;
      }

      /**
       * Method for setting the info field for a vertex
       * @param info
       */
      public void setVertexInfo(int info) {
         this.info = info;
      }

      /**
       * creates a list of outgoing arcs in a graph
       */
      private void createArcList() {
         Arc a = firstArc;
         if (a != null) {
            vertexArcList.add(a);
         }
         while (a.nextArc != null) {
            vertexArcList.add(a.nextArc);
            a = a.nextArc;
         }
      }

      /**
       * Method for printing out outgoing arcs in a vertex. *For testing purposes
       */
      private void printArcList() {
         for (Arc item : vertexArcList) {
            System.out.println(item.toString() + " ");
         }
      }

      /**
       * Method for setting the previousVertex value of a vertex.
       * @param previousVertex
       */
      public void setPreviousVertex(Object previousVertex) {
         previousVertex = previousVertex;
      }

      /**
       * Method for accessing the previousVertex of a vertex.
       * @return previousVertex
       */
      public Vertex getPreviousVertex() {
         return previousVertex;
      }

   }

      /**
       * Arc represents one arrow in the graph. Two-directional edges are
       * represented by two Arc objects (for both directions).
       */
      class Arc {

         private String arcID;
         private Vertex targetVertex;
         private Arc nextArc;
         private int info = 0;
         // You can add more fields, if needed

         Arc(String s, Vertex v, Arc a) {
            arcID = s;
            targetVertex = v;
            nextArc = a;

         }

         Arc(String s) {
            this(s, null, null);
         }

         @Override
         public String toString() {
            return arcID;
         }

         // TODO!!! Your Arc methods here!

         /**
          * Method for setting the value of info in an Arc.
          * @param info
          */
         public void setArcInfo(int info) {
            this.info = info;
         }

         /**
          * Method for getting info from an arc.
          * @return info
          */
         public int getArcInfo() {
            return info;
         }

         /**
          * Method for setting the value of nextArc in an Arc.
          * @param nextArc
          */
         public void setArcNextArc(Arc nextArc){
            this.nextArc = nextArc;
         }


      }


      class Graph {

         private String graphID;
         private Vertex firstVertex;
         private int graphInfo = 0;
         // You can add more fields, if needed
         private List<Vertex> graphVertexList = new ArrayList<>();

         Graph(String s, Vertex v) {
            graphID = s;
            firstVertex = v;
         }

         public void setFirstVertex(Vertex firstVertex) {
            this.firstVertex = firstVertex;
         }

         Graph(String s) {
            this(s, null);
         }

         @Override
         public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(graphID);
            sb.append(nl);
            Vertex v = firstVertex;
            while (v != null) {
               sb.append(v.toString());
               sb.append(" -->");
               Arc a = v.firstArc;
               while (a != null) {
                  sb.append(" ");
                  sb.append(a.toString());
                  sb.append(" (");
                  sb.append(v.toString());
                  sb.append("->");
                  sb.append(a.targetVertex.toString());
                  sb.append(")");
                  a = a.nextArc;
               }
               sb.append(nl);
               v = v.nextVertex;
            }
            return sb.toString();
         }

         public Vertex createVertex(String vid) {
            Vertex newVertex = new Vertex(vid);
            newVertex.nextVertex = firstVertex;
            firstVertex = newVertex;
            return newVertex;
         }

         public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc newArc = new Arc(aid);
            newArc.nextArc = from.firstArc;
            from.firstArc = newArc;
            newArc.targetVertex = to;
            return newArc;
         }


         /**
          * Create a connected undirected random tree with n vertices.
          * Each new vertex is connected to some random existing vertex.
          *
          * @param n number of vertices added to this graph
          */
         public void createRandomTree(int n) {
            if (n <= 0)
               return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
               varray[i] = createVertex("v" + String.valueOf(n - i));
               if (i > 0) {
                  int vnr = (int) (Math.random() * i);
                  createArc("a" + varray[vnr].toString() + "_"
                          + varray[i].toString(), varray[vnr], varray[i]);
                  createArc("a" + varray[i].toString() + "_"
                          + varray[vnr].toString(), varray[i], varray[vnr]);
               } else {
               }
            }
         }

         /**
          * Create an adjacency matrix of this graph.
          * Side effect: corrupts graphInfo fields in the graph
          *
          * @return adjacency matrix
          */
         public int[][] createAdjMatrix() {
            graphInfo = 0;
            Vertex v = firstVertex;
            while (v != null) {
               v.info = graphInfo++;
               v = v.nextVertex;
            }
            int[][] res = new int[graphInfo][graphInfo];
            v = firstVertex;
            while (v != null) {
               int i = v.info;
               Arc a = v.firstArc;
               while (a != null) {
                  int j = a.targetVertex.info;
                  res[i][j]++;
                  a = a.nextArc;
               }
               v = v.nextVertex;
            }
            return res;
         }

         /**
          * Side effect: arc.info values not created - currently does not work correctly with findFurthestFrom method
          * Create a connected simple (undirected, no loops, no multiple
          * arcs) random graph with n vertices and m edges.
          *
          * @param n number of vertices
          * @param m number of edges
          */
         public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
               return;
            if (n > 2500)
               throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
               throw new IllegalArgumentException
                       ("Impossible number of edges: " + m);
            firstVertex = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = firstVertex;
            int c = 0;
            while (v != null) {
               vert[c++] = v;
               v = v.nextVertex;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
               int i = (int) (Math.random() * n);  // random source
               int j = (int) (Math.random() * n);  // random targetVertex
               if (i == j)
                  continue;  // no loops
               if (connected[i][j] != 0 || connected[j][i] != 0)
                  continue;  // no multiple edges
               Vertex vi = vert[i];
               Vertex vj = vert[j];
               createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
               connected[i][j] = 1;
               createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
               connected[j][i] = 1;
               edgeCount--;  // a new edge happily created
            }
         }


         // TODO!!! Your Graph methods here! Probably your solution belongs here.

         /**
          * creates a list of all vertices in a graph
          */
         private void createVertexList() {
            Vertex v = firstVertex;
            if (v != null) {
               graphVertexList.add(v);
            }
            while (v.nextVertex != null) {
               graphVertexList.add(v.nextVertex);
               v = v.nextVertex;
            }
         }

         /**
          * Method for printing graphVertexList *for testing purposes.
          */
         private void printVertexList() {
            for (Vertex item : graphVertexList) {
               System.out.print(item.toString() + " ");
               for (Vertex neighbour : item.vertexNeighbours) {
                  System.out.print(neighbour.toString() + " ");
               }
               System.out.println();
            }
            System.out.println();
         }

         /**
          * The following method is based on an example of using Dijkstras algorithm by Jaanus Pöial.
          * Source: http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
          * Availability last checked 11.22.17
          */

         /**
          * finds the furthest vertex from given vertex
          * @param source the vertex that you want to find the furthest vertex from
          * @return the furthest vertex from source
          */
         private Vertex findFurthestFrom(Vertex source) {
            if(this.graphVertexList.size() == 0){
               throw new RuntimeException("This is not a graph: " + this);
            }
            if (source == null) {
               throw new RuntimeException("The source vertex " + source + ", You have given is not a part of this graph!");
            }
            if (source.vertexNeighbours.isEmpty() & this.graphVertexList.size() == 1) {
               System.out.println("Vertice " + source + " has no neighbouring vertices, and is therefor " +
                       "the furthest vertice from itself.");
               return source;
            }
            int INFINITY = Integer.MAX_VALUE / 4; //Its large enough for intended purposes!
            Iterator vit = this.graphVertexList.listIterator();
            // System.out.println("------------------------ 1st while cycle --------------------------"); // testing purposes
            // System.out.println("Setting distance from source to zero and previous vertex to null!"); // testing purposes
            while (vit.hasNext()) {
               Vertex v = (Vertex) vit.next();
               v.setVertexInfo(INFINITY);
               v.setPreviousVertex(null);
               // System.out.println("Setting vertice " + v + " distance from " + source + " to " + v.info + " and previous vertex to " + v.previousVertex ); // testing purposes
            }
            // System.out.println(" "); // testing purposes
            // System.out.println("Set source distance from itself to 0"); // testing purposes
            source.setVertexInfo(0);
            // System.out.println("Setting " + source + " distance from " + source + " to " + source.info); testing purposes
            // System.out.println(" "); // testing purposes
            List vq = Collections.synchronizedList(new LinkedList());
            vq.add(source);
            while (vq.size() > 0) {
               // System.out.println("------------------------- 2nd while cycle -------------------------------"); // testing purposes
               // System.out.println("Resetting minimum length and minimum vertex values."); // testing purposes
               int minLen = INFINITY;
               // System.out.println("Setting minumum length to " + minLen); // testin purposes
               Vertex minVert = null;
               // System.out.println("Setting minimum vertex to " + minVert); // testing purposes
               // System.out.println(" "); // testing purposes
               Iterator it = vq.iterator();
               while (it.hasNext()) {
                  // System.out.println("------------------------- 3rd while cycle -------------------------"); // testin purposes
                  Vertex v = (Vertex) it.next();
                  // System.out.println("Setting minumum vertex!"); // testing purposes
                  // System.out.println("Minimum vertex " + v  + " is " + v.info + " distance from " + source); // testin purposes
                  if (v.getVertexInfo() < minLen) {
                     minVert = v;
                     minLen = v.getVertexInfo();
                     // System.out.println("Former was further than " + v + " with distance of " + v.getVertexInfo()); // testing purposes
                     // System.out.println(" "); // testing purposes
                  }
               }
               if (minVert == null) {
                  throw new RuntimeException("The findFurthestFrom method has encountered an error. " + minVert
                          + " Is non existant! Why? We do not know.");
               }
               if (vq.remove(minVert)) {
                  // System.out.println(minVert + " " + minLen); // testin purposes
               } else {
                  throw new RuntimeException("The findFurthestFrom method has enountered an error" +
                          minVert + " could not be removed from stack!" + "We are now in an endless loop!");
               }
               minVert.createArcList();
               it = minVert.vertexArcList.listIterator();
               // System.out.println("Created " + minVert + "'s arclist consisting of " + minVert.vertexArcList); // testing purposes
               // System.out.println(" "); // testing purposes
               while (it.hasNext()) {
                  // System.out.println("-------------------------- 4th while cycle ------------------------------"); // testing purposes
                  Arc a = (Arc) it.next();
                  int newLen = minVert.getVertexInfo() + a.getArcInfo();
                  Vertex to = a.targetVertex;
                  // System.out.println("This arc " + a + " is " +  a.info + " long. " + " New length to next vertex + to + " is " + newLen); // testing purposes
                  if (to.getVertexInfo() == INFINITY) {
                     vq.add(to);
                  }
                  if (newLen < to.getVertexInfo()) {
                     to.setVertexInfo(newLen);
                     to.setPreviousVertex(minVert);
                  }
               }
            }
            // System.out.println("---------------------- 5th cycle --------------------------"); //testing purposes
            // System.out.println("Finding the furthest Vertex after Djikstra."); // testing purposes
            Vertex maxVert = source;
            for (Vertex item : graphVertexList) {
               if (item.vertexID == source.vertexID){
               item.setVertexInfo(0);
               }
               // System.out.println("Vertice " + item + " distance from " + source + " is " +item.info); // testing purposes
               if(maxVert.getVertexInfo() < item.getVertexInfo()){
                  maxVert = item;
               }
            }
            // System.out.println(" "); //testing purposes
             System.out.println(maxVert); // testing purposes
            return maxVert;
         }

      }
} 

